def custom_score(game, player):
    """Calculate the heuristic value of a game state from the point of view
    of the given player.
    This should be the best heuristic function for your project submission.
    Note: this function should be called from within a Player instance as
    `self.score()` -- you should not need to call this function directly.
    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).
    player : object
        A player instance in the current game (i.e., an object corresponding to
        one of the player objects `game.__player_1__` or `game.__player_2__`.)
    Returns
    -------
    float
        The heuristic value of the current game state to the specified player.
    """
    player_moves = game.get_legal_moves(player)
    player_moves_len = len(player_moves)
    p_score = position_score(player_moves)
    opponent_moves_len = len(game.get_legal_moves(game.get_opponent(player)))
    return float(0.7 * player_moves_len - opponent_moves_len + 1.3 * p_score)


def custom_score_2(game, player):
    """Calculate the heuristic value of a game state from the point of view
    of the given player.
    Note: this function should be called from within a Player instance as
    `self.score()` -- you should not need to call this function directly.
    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).
    player : object
        A player instance in the current game (i.e., an object corresponding to
        one of the player objects `game.__player_1__` or `game.__player_2__`.)
    Returns
    -------
    float
        The heuristic value of the current game state to the specified player.
    """
    return position_score(game.get_legal_moves(player))


def custom_score_3(game, player):
    """Calculate the heuristic value of a game state from the point of view
    of the given player.
    Note: this function should be called from within a Player instance as
    `self.score()` -- you should not need to call this function directly.
    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).
    player : object
        A player instance in the current game (i.e., an object corresponding to
        one of the player objects `game.__player_1__` or `game.__player_2__`.)
    Returns
    -------
    float
        The heuristic value of the current game state to the specified player.
    """
    player_moves_len = len(game.get_legal_moves(player))
    opponent_moves_len = len(game.get_legal_moves(game.get_opponent(player)))
    return float(0.7 * player_moves_len - opponent_moves_len)


# number of wins achieved depending on a move taken
move_wins = {"1,2": 2777, "3,3": 4996, "1,4": 2921, "3,5": 3442, "1,6": 1290, "2,4": 3848, "4,3": 4460, "6,2": 1971, "4,1": 2819, "6,0": 532, "5,2": 3146, "6,4": 2302, "4,5": 3457, "2,6": 2114, "0,5": 1257, "4,2": 3835, "3,4": 4412, "0,2": 1827, "1,0": 1086, "2,2": 3186, "3,0": 1778, "5,1": 2149, "5,0": 1211, "3,1": 2968, "5,6": 1468, "5,4": 3407, "0,3": 1822, "0,4": 2005, "2,5": 3186, "4,6": 2304, "6,5": 1514, "5,3": 3415, "5,5": 2383, "6,3": 2082, "6,1": 1265, "3,6": 2138, "4,4": 4273, "2,3": 4365, "2,1": 2717, "1,3": 2875, "0,6": 519, "4,0": 1889, "3,2": 4165, "6,6": 616, "0,1": 1101, "2,0": 1791, "1,5": 2202, "1,1": 2021, "0,0": 444}
matches_simulated = 15600
def position_score(moves):
    total = 0
    for move in moves:
        key = str(move[0]) + ',' + str(move[1])
        total += move_wins[key] / float(matches_simulated)

    return total